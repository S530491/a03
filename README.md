# Sirisha's Guestbook 

A simple guest book using Node, Express, BootStrap, EJS

## How to use

Open a command window in your c:\44563\w07 folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```
# About My Website
Point your browser to `http://localhost:8081`. 
My website consists of 3 webpages.
First page is about my self Introduction, it contains my education, interests and goal details.
Second page is for calculation of reverse of the number and check whether the given number is palindrome or not. 
This will take number input and give the result. 
Third page is for contact form. It contains the fields like FirstName, LastName,Email and Question.
In the third page if we given the mail id and click on the submit button then mail gun will get triggered and message will be received by respective mail id.
