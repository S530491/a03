QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 == "1", "1=1 success!");
});
QUnit.test('Testing our cal function with four sets of inputs', function (assert) {
    assert.strictEqual(cal(4,4,4), "4.00", 'Top Grade');
    assert.strictEqual(cal(3,3,3), "3.00", 'Average Grade');
    assert.strictEqual(cal(4,3,3), "3.33", 'Good Grade');
    assert.strictEqual(cal(0,0,0), "0.00", 'Fail');
   
});
QUnit.test('Testing our gradecal function with five sets of inputs', function (assert) {
    assert.strictEqual(gradcal('A'), 4, 'Grade A');
    assert.strictEqual(gradcal('B'), 3, 'Grade B');
    assert.strictEqual(gradcal('C'), 2, 'Grade C');
    assert.strictEqual(gradcal('D'), 1, 'Grade D');
    assert.strictEqual(gradcal('F'), 0, 'Grade F');
});


