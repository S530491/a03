var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser"); // simplifies access to request body

var app = express();  // make express app
var http = require('http').createServer(app);  // inject app into the server
// set up the view engine
app.set("views", path.resolve(__dirname, "assets")); // path to views
app.set("view engine", "ejs"); // specify our view engine
app.use(express.static(__dirname + '/assets'));
// manage our entries
var entries = [];
app.locals.entries = entries; // now entries can be accessed in .ejs files
// set up the logger
app.use(logger("dev")); // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }));
// GETS
app.get("/", function (request, response) {
  response.sendFile(__dirname+"/assets/about.html");
});
app.get("/about", function (request, response) {
    response.sendFile(__dirname+"/assets/about.html");
});
app.get("/MathFun", function (request, response) {
    response.sendFile(__dirname+"/assets/MathFun.html");
});
app.get("/contact", function (request, response) {
    response.sendFile(__dirname+"/assets/contact.html");
});

// 1 set up the view engine
// 2 manage our entries
// 3 set up the logger
// 4 handle valid GET requests
// 5 handle valid POST request
// 6 respond with 404 if a bad URI is requested

// Listen for an application request on port 8081

app.get("/guestbook", function (request, response) {
    response.render("index");
  });


app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");  // where to go next? Let's go to the home page :)
});
app.post("/contact", function (request, response) {
  var api_key = 'key-1354c472979269b34837c06e0a8e2fb6';
  var domain = 'sandboxad715776f82446bfa6dbe82d1e00606d.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'Mail Gun <postmaster@sandboxad715776f82446bfa6dbe82d1e00606d.mailgun.org>',
    to: 'siri94.vinukonda@gmail.com',
    subject: request.body.firstname,
    text: request.body.message
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error)
      response.send("Mail sent!");
    else
      response.send("Mail not sent!");
  });
});
http.listen(8081, function () {
    console.log('Guestbook app listening on http://127.0.0.1:8081/')
  });
// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404");
});

// Listen for an application request on port 8081 & notify the developer
//http.listen(8081, function () {
 // console.log('Guestbook app listening on http://127.0.0.1:8081/')
//})


